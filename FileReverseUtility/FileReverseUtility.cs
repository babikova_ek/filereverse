﻿using System;
using System.IO;
using System.Linq;

namespace FileReverseUtility
{
    class Utility
    {
        private string[] args;
        private readonly string _sourceFileName;
        private readonly string _targetFileName;

        static void Main(string[] args)
        {
            var utility = new Utility(args);

            try
            {
                utility.Execute();
            }
            catch (Exception exception)
            {
                Console.WriteLine("An error has occurred: {0}", exception.Message);
            }
        }

        public Utility(string[] args)
        {
            this.args = args;

            ensureWeHaveEnoughArgs();

            _sourceFileName = args[0];
            _targetFileName = args[1];
        }

        private void ensureWeHaveEnoughArgs()
        {
            if (args.Length < 2)
            {
                throw new Exception("Wrong number of arguments.");
            }
        }

        private void Execute()
        {
            if (HelpRequested())
            {
                PrintUsage();

                return;
            }

            CopyData();
        }

        public void CopyData()
        {
            if (Algorithm.CanCopySequentially(_sourceFileName, _targetFileName))
            {
                Algorithm.CopyDataSequentially(_sourceFileName, _targetFileName);
            }
            else
            {
                bool confirmed = RequestUnsafeMethodConfirmation();

                if (confirmed)
                {
                    Algorithm.CopyDataViaInMemoryBuffer(_sourceFileName, _targetFileName);
                }
            }
        }

        private bool HelpRequested()
        {
            foreach (var arg in args)
            {
                if (arg == "-?" || arg == "-h" || arg == "--help")
                {
                    return true;
                }
            }

            return false;
        }

        private void PrintUsage()
        {
            var executableFileName = System.AppDomain.CurrentDomain.FriendlyName;

            Console.WriteLine("This utility copies bytes frome one file to another in the reversed order.");
            Console.WriteLine("Usage: " + executableFileName + "<source file> <target file> ");
            Console.WriteLine("Where: <source file> -- an original file to copy data from");
            Console.WriteLine("       <target file> -- a file where the reversed bytes will be stored");
            Console.WriteLine("                        (if the target file does not exist, it will be created)");
        }

        private bool RequestUnsafeMethodConfirmation()
        {
            Console.WriteLine("Specified files do not support seek operation.");
            Console.WriteLine("Copy can be performed via in-memory buffer (can be unsafe with large files).");

            Console.Write("Would you like to proceed? (yes|no): ");

            return Console.ReadLine().ToLower() == "yes";
        }
    }
}
