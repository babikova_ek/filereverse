﻿using System.IO;
using System.Linq;

namespace FileReverseUtility
{
    public static class Algorithm
    {
        public static bool CanCopySequentially(string sourceFile, string targetFile)
        {
            using (var sourceFileStream = new FileStream(sourceFile, FileMode.Open))
            {
                if (!sourceFileStream.CanSeek)
                {
                    return false;
                }
            }

            return true;
        }

        public static void CopyDataSequentially(string sourceFile, string targetFile)
        {
            using (var sourceFileStream = new FileStream(sourceFile, FileMode.Open))
            {
                var fileSize = GetFileSize(sourceFileStream);

                using (var targetFileStream = new ReverseDecorator(new FileStream(targetFile, FileMode.OpenOrCreate), fileSize))
                {
                    var endOfStream = -1;

                    while (true)
                    {
                        var oneByte = sourceFileStream.ReadByte();

                        if (oneByte == endOfStream)
                        {
                            break;
                        }

                        targetFileStream.WriteByte(oneByte);
                    }
                }
            }
        }

        private static long GetFileSize(FileStream fileStream)
        {
            var fileSize = fileStream.Seek(0, SeekOrigin.End) - 1;

            fileStream.Seek(0, SeekOrigin.Begin);

            return fileSize;
        }

        public static void CopyDataViaInMemoryBuffer(string sourceFile, string targetFile)
        {
            var bytes = File.ReadAllBytes(sourceFile).Reverse().ToArray();

            File.WriteAllBytes(targetFile, bytes);
        }
    }
}
