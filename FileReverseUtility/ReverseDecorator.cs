﻿using System;
using System.IO;

namespace FileReverseUtility
{
    public class ReverseDecorator : IDisposable
    {
        private FileStream _fileStream;

        public ReverseDecorator(FileStream fileStream, long fileSize)
        {
            _fileStream = fileStream;

            fileStream.SetLength(fileSize);

            fileStream.Seek(0, SeekOrigin.End);
        }

        public void Dispose()
        {
            _fileStream.Dispose();
        }

        public void WriteByte(int value)
        {
            _fileStream.WriteByte((byte)value);

            var offset = -2;
            var currentPosition = _fileStream.Seek(0, SeekOrigin.Current);

            if (currentPosition == 1)
            {
                offset = -1;
            }

            _fileStream.Seek(offset, SeekOrigin.Current);
        }
    }
}
